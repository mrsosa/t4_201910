package model.vo;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> 
{

	// TODO Definir los atributos de una infraccion

	/**
	 * objectId del auto
	 */
	private String objectId;
	/**
	 * violationDescription del auto
	 */
	private String violationDescription;
	/**
	 * location del auto
	 */
	private String location;
	/**
	 * totalPaid del auto
	 */
	private String totalPaid;
	/**
	 * accidentIndicator del auto
	 */
	private String accidentIndicator;
	/**
	 * ticketIssueDate del auto
	 */
	private String ticketIssueDate;


	/**
	 * constructor
	 */
	public VOMovingViolation(String pObjectId,String pViolationDescription,String pLocation,String pTotalPaid,String pAccidentIndicator, String pTicketIssueDate)
	{ 
		objectId=pObjectId;
		violationDescription=pViolationDescription;
		location=pLocation;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		ticketIssueDate=pTicketIssueDate;
	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(objectId);
	}	

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return Integer.parseInt(totalPaid);
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}

	@Override
	public int compareTo(VOMovingViolation o) 
	{
		// TODO implementar la comparacion "natural" de la clase
		int rta=ticketIssueDate.compareTo(o.getTicketIssueDate());
		if(rta==0)
		{
	        rta=objectId.compareTo(o.objectId);
		}
		
		return rta;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "La infraccion: "+objectId+" ocurrio en: "+location+" el dia: "+ticketIssueDate+" se tiene que pagar: "+totalPaid +" y tiene descripcion: "+violationDescription;
	}
}
